const taos = require("@tdengine/client");

var host = null;
var port = 6030;
for (var i = 2; i < global.process.argv.length; i++) {
  var key = global.process.argv[i].split("=")[0];
  var value = global.process.argv[i].split("=")[1];

  if ("host" == key) {
    host = value;
  }
  if ("port" == key) {
    port = value;
  }
}

if (host == null) {
  console.log("Usage: node nodejsChecker.js host=<hostname> port=<port>");
  process.exit(0);
}

// establish connection
var conn = taos.connect({
  host: host,
  user: "root",
  password: "taosdata",
  port: port,
});
var cursor = conn.cursor();
// create database
// use db
executeSql("use solar", 0);
// select
executeQuery(`select 
        station_id,
        control_id,
        last(*)
        from records 
        GROUP BY station_id, control_id`);
// close connection
conn.close();

function executeQuery(sql) {
  var start = new Date().getTime();
  var promise = cursor.query(sql, true);
  var end = new Date().getTime();
  promise.then(function (result) {
    printSql(sql, result != null, end - start);

    console.log(JSON.stringify(result, null, 2));
    const fields = result.fields.map((i) =>
      i.name.replace(/last\((.*?)\)/g, "$1")
    );
    console.log(
      result.data.map((i) =>
        i.data.reduce((prev, curr, index) => {
          prev[fields[index]] = curr;
          return prev;
        }, {})
      )
    );
  });
}

function executeSql(sql, affectRows) {
  var start = new Date().getTime();
  var promise = cursor.execute(sql);
  var end = new Date().getTime();
  printSql(sql, promise == affectRows, end - start);
}

function printSql(sql, succeed, cost) {
  console.log(
    "[ " +
      (succeed ? "OK" : "ERROR!") +
      " ] time cost: " +
      cost +
      " ms, execute statement ====> " +
      sql
  );
}
