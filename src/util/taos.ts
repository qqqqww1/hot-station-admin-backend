const taos = require("@tdengine/client");

const getConnection = () => {
    return taos.connect({
        host: "39.106.131.169",
        port: 6030,
        user: "root",
        password: "taosdata",
    })
}

const conn = getConnection();

export const execSql = async (sql: string) => {
    const cursor = conn.cursor();
    cursor.execute('use solar')
    const result = await cursor.query(sql, true);
    cursor.close();

    const fields = result.fields.map((i: any) =>
        i.name.replace(/last\((.*?)\)/g, "$1")
    );
    return result.data.map((i: any) =>
        (i.data as any[]).reduce((prev, curr, index) => {
            prev[fields[index]] = curr;
            return prev;
        }, {} as Record<string, any>)
    )
}