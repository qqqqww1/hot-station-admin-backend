import { FastifyInstance } from "fastify";
import infoController from "./controller/infoController";

export default async function router (fastify: FastifyInstance) {
  fastify.register(infoController, { prefix: "/" });
}
