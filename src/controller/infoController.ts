import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify";
import { PrismaClient } from '@prisma/client'
import { execSql } from "../util/taos";

export default async function infoController (fastify: FastifyInstance) {
    const prisma = new PrismaClient()

    fastify.get("/stations", async function (
        _request: FastifyRequest,
        reply: FastifyReply
    ) {
        const data = await prisma.station.findMany({
            include: {
                station_detail: true,
                station_plan: true,
                control_bin: true
            }
        })
        const taosData = await execSql(
            `
        select 
        station_id,
        control_id,
        last(*)
        from records 
        GROUP BY station_id, control_id`
        )

        reply.send(data.map(item => ({
            ...item,
            record: taosData.filter((t: any) => t.station_id === item.id)
        })))
    });
}
