FROM node:18-bullseye as build

ADD TDengine-client-3.0.5.1-Linux-x64.tar.gz /opt/

WORKDIR /opt/TDengine-client-3.0.5.1
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo 'Asia/Shanghai' >/etc/timezone && \
    /bin/bash install_client.sh

WORKDIR /app
COPY package.json .

# yarn 国内源
RUN yarn config set registry https://registry.npmmirror.com
RUN yarn install

COPY . .
RUN yarn prisma generate
RUN yarn build

EXPOSE 3006
CMD ["node", "dist/index.js"]
